package ru.inordic.newMySuperLinkedList;

import java.util.Scanner;

public class MyLinkedListWork {
    public static void main(String[] args) {

        MyLinkedList myMist = new MyLinkedList();
        Scanner scanner = new Scanner(System.in);
        String x;
        System.out.println("Эта штука умеет добавлять цифры");
        System.out.println("пиши тут");
        System.out.println("Введи номер операции\n" +
                "1-добавить\n" +
                "2-добавить по индексу\n" +
                "3-удалить из указанной позиции\n" +
                "4-получить размер списка\n" +
                "5-получить значение элемента по позиции\n" +
                "6-показать весь список") ;
        do {

            x = scanner.nextLine();

            int a = 0;
            int b = 0;
            switch (x) {
                case "1":
                    System.out.println("добавить");
                    a = scanner.nextInt();
                    myMist.addLast(a);
                    break;
                case "2":
                    System.out.println("введи что добавить");
                    a = scanner.nextInt();
                    System.out.println("введи индекс");
                    b = scanner.nextInt();
                    myMist.add(a, b);
                    break;
                case "3":
                    System.out.println("введи индекс");
                    a = scanner.nextInt();
                    myMist.removeElementByPosition(a);
                    break;
                case "4":
                    System.out.println(myMist.getSize());
                    break;
                case "5":
                    System.out.println("введи индекс");
                    a = scanner.nextInt();
                    myMist.getElementByPosition(a);
                    break;
                case "6":
                    myMist.print();
                    break;
                default:
                    break;
            }

        } while (!x.equals("exit"));


//        myMist.addFirst(12355);
//        myMist.addFirst(12344);
//        myMist.addFirst(12333);
//        myMist.addFirst(12322);
//        myMist.addFirst(12311);
//        myMist.addFirst(12311);
//        myMist.addFirst(12311);
//        myMist.addFirst(12311);
//        myMist.addFirst(12311);
//        myMist.addFirst(12311);
//
//        myMist.addLast(11);
//        myMist.addLast(22);
//        myMist.addLast(33);
//        myMist.addLast(44);
//        myMist.addLast(55);
//        myMist.addLast(66);
//
//        myMist.add(10, 1);
//        myMist.add(20, 5);
//        myMist.add(30, 7);
//        myMist.add(40, 9);
//
//        myMist.print();
//        System.out.println(myMist.getSize());
//
////        myMist.removeFirst();
////        myMist.removeFirst();
//        myMist.removeElementByPosition(1);
//        myMist.removeElementByPosition(5);
//        System.out.println();
//
//        myMist.print();
//        System.out.println(myMist.getSize());

    }
}
