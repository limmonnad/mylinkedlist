package ru.inordic.newMySuperLinkedList;

// Спроектировать класс, который реализует двунаправленный
// связанный список для целых чисел.
//
//        Класс должен поддерживать методы:
//
//      1  добавить элемент (в конец) +
//      2  добавить элемент (в позицию) +
//      3  удалить элемент из указанной позиции; +
//      4  получить размер списка;+
//      5  получить значение элемента по позиции;+
//        При указании неверного параметра позиции метод должен
//        бросать исключение;
//
//        Программа создаёт экземпляр данного класса и
//        просит пользователя ввести выбрать действие для
//        работы (или выход) в цикле. Если в результате
//        исполнения действия возникло исключения (в том числе
//        некорректного ввода самого числа),
//        оно должно быть обработано корректно.



public class MyLinkedList implements MyList {

    Node first = null;
    Node last = null;
    int size = 0;


    public boolean isEmpty() {
        return first == null;
    }

    @Override
    public void addFirst(int value) {
        Node newNode = new Node(value);
        if (isEmpty()) {
            last = newNode;
        } else {
            first.prev = newNode;
        }
        newNode.next = first;
        first = newNode;
        size++;


    }

    @Override
    public void addLast(int value) {
        Node newNode = new Node(value);
        if (isEmpty()) {
            first = newNode;
        } else {
            last.next = newNode;
        }
        newNode.prev = last;
        last = newNode;


        size++;
    }

    public void print() {
        Node newNode = first;
        while (newNode != null) {
            System.out.println(newNode.value);
            newNode = newNode.getNext();
        }
    }


    @Override
    public void add(int value, int position) {
        Node temp = first;
        int x = 0;
        while (temp != null && x != position) {
            temp = temp.getNext();
            x++;
        }
        Node newNode = new Node(value);

        temp.prev.next = newNode;
        newNode.prev = temp.prev;
        temp.prev = newNode;
        newNode.next = temp;
        size++;
    }

    public void removeFirst() {
        if (first.next == null) {
            last = null;
        } else
            first.next.prev = null;

        first = first.next;
        size--;
    }

    public void removeLast() {
        if (first.next == null) {
            first = null;
        } else
            last.prev.next = null;

        last = last.prev;
        size--;
    }

    @Override
    public void removeElementByPosition(int position) {
        Node temp = first;
        int x = 0;
        while (x != position) {
            temp = temp.next;
            x++;
            if (temp == null) {
                return;
            }
        }

        if (temp == first)
            removeFirst();
        else temp.prev.next = temp.next;

        if (temp == last) {
            removeLast();
        } else temp.next.prev = temp.prev;

        size--;
    }

    @Override
    public int getSize() {
        return size;
    }

    @Override
    public int getElementByPosition(int position) {
        Node x = first.getNext();
        for (int i = 0; i < position; i++) {
            if (x == null) {
                throw new IllegalArgumentException();
            }
            x = x.getNext();
        }
        return x.getValue();
    }


    //===================class Node
    static class Node {

        public int value;
        Node next;
        Node prev;

        public Node(int value) {
            this.value = value;
        }


        public Node(int value, Node prev, Node next) {
            this.value = value;
            this.prev = prev;
            this.next = next;
        }

        public int getValue() {
            return value;
        }

        public void setValue(int value) {
            this.value = value;
        }

        public Node getNext() {
            return next;
        }

        public void setNext(Node next) {
            this.next = next;
        }

        public Node getPrev() {
            return prev;
        }

        public void setPrev(Node prev) {
            this.prev = prev;
        }


    }

}








//    public MyLinkedList() {
//        first = null;
//        last = null;
//
////        first = new Node(0, null, last);
////        last = new Node(0, first, null);
//    }
//        Node newNode = first;
//        newNode.setValue(value);
//        first = new Node(0, null, newNode);
//        newNode.setPrev(first);
//        size++;
//        Node newNode = last;
//        newNode.setValue(value);
//        last = new Node(0, newNode, null);
//        newNode.setNext(last);
//        size++;
//
//    @Override
//    public void addFirst(int value) {
//        if (first.getNext() == null) {
//            Node newNode = first;
//            newNode.setValue(value);
//            first = new Node(0, null, newNode);
//            newNode.setPrev(first);
//            ++size;
//        }
//
//    }
//
//    @Override
//    public void addLast(int value) {
//        if (size == 0) {
//            Node newNode = last;
//            newNode.setValue(value);
//            last = new Node(0, newNode, null);
//            newNode.setNext(last);
//            ++size;
//        } else {
//
//        }
//    }





//======================================================================================

//
//    Node first = null;
//    Node last = null;
//    int size = 0;
//
//     public MyLinkedList() {
//        this.first = new Node(0, last, null);
//        this.last = new Node(0, null, first);
//    }
//
//    @Override
//    public void addFirst(int value) {
//        Node node = first;
//        node.setValue(value);
//        first = new Node(0, node, null);
//        node.setPrev(first);
////        first.setNext(node);
//        size++;
//    }
//
//
//    @Override
//    public void addLast(int value) {
//        Node node = last;
//        node.setValue(value);
//        last = new Node(0, null, node);
//        node.setNext(last);
////        last.setPrev(node);
//        size++;
//    }
//
//    @Override
//    public void add(int value, int position) {
////        if(position>size){
////            throw new UnsupportedOperationException();
////        }
////        else if (first.getNext()==null){
////            addFirst(value);
////        }
////        else {
////            for (int i = 0; i < position; i++) {
////
////
////            }
////
////        }
////        Node node = first;
////        node.setValue(value);
////
//    }
//
//
//    @Override
//    public void removeElementByPosition(int position) {
//
//    }
//
//    @Override
//    public int getSize() {
//        return size;
//    }
//
//    @Override
//    public int getElementByPosition(int position) {
//        Node node = first.getNext();
//        for (int i = 0; i < position; i++) {
//            node = node.getNext();
//        }
//        return node.getValue();
//    }
//
//    public Node getNextElement(Node x) {
//        return x.getNext();
//    }
//

//    @Override
//    public void add(int value, int position) {
//        Node newNode = new Node();
//        newNode.setValue(value);
//        if (position == 0) {
//            if (first == null) {
//                first = newNode;
//            } else {
//                newNode.setNext(first);
//                first.setPrev(newNode);
//            }
//        } else if (position == size) {
//            Node previous = findByPosition(position - 1);
//            newNode.setNext(last);
//            last.setPrev(newNode);
//            newNode.setPrev(previous);
//            previous.setNext(newNode);
//        } else {
//            Node previous = findByPosition(position - 1);
//            Node nexttt = findByPosition(position + 1);
//            newNode.setPrev(previous);
//            previous.setNext(newNode);
//            newNode.setNext(nexttt);
//            nexttt.setPrev(newNode);
//        }
//    }
//
//    //
//    public Node findByPosition(int position) {
//        Node x = first;
//        for (int i = 0; i < position; i++) {
//            if (x == null) {
//                throw new IllegalArgumentException();
//            }
//            x = x.getNext();
//        }
//        return x;
//    }


