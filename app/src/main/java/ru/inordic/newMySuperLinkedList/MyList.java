package ru.inordic.newMySuperLinkedList;

//      1  добавить элемент (в конец)
//      2  добавить элемент (в позицию)
//      3  удалить элемент из указанной позиции;
//      4  получить размер списка;
//      5  получить значение элемента по позиции;

public interface MyList {


    void addFirst(int value);

    void addLast(int value);

    void add (int value, int position);

    void removeElementByPosition(int position);

    int getSize();

    int getElementByPosition(int position);

}
