package ru.inordic.doubleList;

import java.util.LinkedList;

public class DoubleList implements Linked {


    private Node first;
    private Node last;
    private int size = 0;

    public DoubleList() {
        last = new Node(0, first, null);
        first = new Node(0, null, last);
    }

    @Override
    public void addFirst(int a) {



//        Node tmp = first;
//        first.setCurrentElement(a);
//        first = new Node(0, null, tmp);
//        tmp.setNext(first);
        size++;

    }

    @Override
    public void addLast(int a) {

        Node tmp = last;
        last.setCurrentElement(a);
        last = new Node(0, tmp, null);
        tmp.setNext(last);
        size++;

//
//        Node newNode = new Node(a,first,)
//
//        if (isEmpty()) {
//            head = newNode;
//        } else {
//            tail.next = newNode;
//            newNode.previous = tail;
//        }
//        tail = newNode;
//        length++;




//        if (first == null) {
//            this.first = new Node(a, first, last);
//
//        }else{
//
//        }




//        Node l = last;
//        Node newNode = new Node(a, l, null);
//        last = newNode;
//        if (l == null) {
//            first = newNode;
//        } else{
//            l.next = newNode;
//        }
//        size++;




    }


    @Override
    public int getSize() {
        return 0;
    }


    @Override
    public int getByIndex(int a) {
        Node tmp = first.getNext();
        for (int i = 0; i < a; i++) {
            tmp = getNextElement(tmp);
        }
        return tmp.getCurrentElement();
    }


    ///////////////////////////////////////////////////////////////////////////////////////
    @Override
    public void removeByIndex(int a) {

    }

    private Node getNextElement(Node node) {
        return node.getNext();
    }


    private static class Node {

        private int currentElement;
        private Node next;
        private Node prev;

        public Node(int currentElement, Node prev, Node next) {
            this.currentElement = currentElement;
            this.next = next;
            this.prev = prev;
        }


        public int getCurrentElement() {
            return currentElement;
        }

        public void setCurrentElement(int currentElement) {
            this.currentElement = currentElement;
        }

        public Node getNext() {
            return next;
        }

        public void setNext(Node next) {
            this.next = next;
        }

        public Node getPrev() {
            return prev;
        }

        public void setPrev(Node prev) {
            this.prev = prev;
        }
    }


    public static void main(String[] args) {
        DoubleList doubleList = new DoubleList();
        doubleList.addLast(454545);

        System.out.println(doubleList.getByIndex(0));
//        System.out.println(doubleList.getByIndex(0));
//        System.out.println(doubleList.getByIndex(1));
//        System.out.println(doubleList.getByIndex(2));
//        System.out.println(doubleList.getByIndex(3));

    }


}
