package ru.inordic.doubleList;

import java.util.Arrays;

public class TestDoubleTest {

    public static void main(String[] args) {
        TestDoubleTest testDoubleTest = new TestDoubleTest();
        testDoubleTest.addLast(1212122);
        testDoubleTest.addLast(1212122);
        testDoubleTest.addLast(43242324);
        testDoubleTest.addLast(1212122);
        testDoubleTest.addLast(4444444);
        System.out.println(testDoubleTest.getSize());
        System.out.println(testDoubleTest.getByIndex(4));

        testDoubleTest.toString();

    }


    private Node first;
    private Node last;
    private int size = 0;

    public TestDoubleTest() {
        last = new Node(0, first, null);
        first = new Node(0, null, last);
    }

    @Override
    public String toString() {
        int[] result = new int[size];
        int index = 0;
        Node temp = first;
        while (temp != null) {
            result[index++] = temp.getCurrentElement();
            temp = temp.getNextElement();
        }
        return Arrays.toString(result);

    }

//    @Override
    public void addFirst(int a) {
        Node pervii = first;
        pervii.setCurrentElement(a);
        first = new Node(0, null, pervii);
        pervii.setPrevElement(first);
        size++;
    }

//    @Override
    public void addLast(int a) {
        Node posledn = last;
        posledn.setCurrentElement(a);
        last = new Node(0, posledn, null);
        posledn.setNextElement(last);
        size++;
    }

//    @Override
    public int getSize() {
        return size;
    }

//    @Override
    public int getByIndex(int a) {
        Node target = first.getNextElement();
        for (int i = 0; i < a; i++) {
            target = getNextElement(target);
        }
        return target.getCurrentElement();
    }

    private Node getNextElement(Node a) {
        return a.getNextElement();
    }


//    @Override
    public void removeByIndex(int a) {


    }


    private class Node {
        private int currentElement;
        private Node nextElement;
        private Node prevElement;

        public Node(int currentElement, Node prevElement, Node nextElement) {
            this.currentElement = currentElement;
            this.prevElement = prevElement;
            this.nextElement = nextElement;
        }

        public int getCurrentElement() {
            return currentElement;
        }

        public void setCurrentElement(int currentElement) {
            this.currentElement = currentElement;
        }

        public Node getNextElement() {
            return nextElement;
        }

        public void setNextElement(Node nextElement) {
            this.nextElement = nextElement;
        }

        public Node getPrevElement() {
            return prevElement;
        }

        public void setPrevElement(Node prevElement) {
            this.prevElement = prevElement;
        }
    }


}
