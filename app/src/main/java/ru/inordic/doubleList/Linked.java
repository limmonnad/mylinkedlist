package ru.inordic.doubleList;

public interface Linked {

    void addFirst(int a);

    void addLast(int a);

    int getSize();

    int getByIndex(int a);

    void removeByIndex(int a);

    String toString();

}
